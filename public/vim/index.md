# Nvim Keybind Help

## Basic
**Normal Mode**
`b` Move to last word
`B` Move to last token
`w` Moves to next word
`W` Moves to next token

`_` Moves to start of line
`%` Moves to send of line

`H` Moves screen to top
`M` Moves screen to middle
`L` Moves screen to bottom

`yy` Copies Line
`dd` Deletes Line
`p` Pastes after cursor
`P` Pastes before the cursor

`:e <file_name>` Adds file to buffer
`:bd` Close Buffer
`:sp <file_name>` Opens file split horizontally
`:vs <file_name>` Opens file split vertically
`:tab ba` Opens all buffers as tabs
`C-W <direction>` Changes the focused window
`gt` Next Tab
`gT` Previous Tab
`C-ws` Split Viewport
`C-wv` SPlit Viewport vertically
`C-wq` Quit viewport
`#=` Indent # of lines


**Visual Mode**

`v` Selects text by character
`V` selects text by line
`C-v` Selects text by block

`aw` Selects word
`ab` Selects block with ()
`aB` Selects block with {}
`at` Selects block with <>
Replace `a` with `i` to select inner block
`u` Changes selected to lowercase
`U` Changes selected to uppercase

## Addons
`<leader>` = " "
`<leader>pf` Telescope Find Files
`C-p` Telescope Find Git Files
`<leader>ps` Telescope find in project
`<leader>pv` Netrw Directory Listing
`<leader>/c` Clear search highlighting
`C-e` Toggle NERDTree
`C-n` New NERDTree
`F8` Toggle Tagbar

**LSP**

`C-p` Previous Suggestion
`C-n` Next Suggestion
`C-y` OR `Tab` Confirm Suggestion 

`gd` GoTo Definition
`k` Hover 
`<leader>vd` View Diagnostics
`[d` Next Diagnostics
`]d` Previous Diagnostics
`<leader>vws` View Workspace Symbols 
`<leader>vca` Code Action
`<leader>vrr` View References
`<leader>vrn` Rename
